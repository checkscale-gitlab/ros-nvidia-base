#!/bin/bash
###############################################################################
#* install.sh
#*
#* This script will help you to install docker and nvidia-docker at your ubuntu
#* PC
#*
#* License BSD - Martin Fees - 2016 (see main directory for full license)
#*******************************************************************************

# Variables for names
VERSION=1.0
AUTHOR="Martin Fees"
EMAIL="martin.fees@gmx.de"

# General variables
ROOT_UID="0"

#*******************************************************************************

#
# Definition of function check_sudo ()
#
# brief: Check if the script is running with sudo

check_sudo ()
{
  #Check if run as root
  if [ "$UID" -ne "$ROOT_UID" ] ; then
    echo "* Error: You've to run this script with sudo!"
  	echo "* Aborting "
    echo "***********************************************************************"
  	exit 1
  fi
}

#*******************************************************************************

#
# Definition of function install_docker ()
#
# brief: Installs the docker engine

install_docker ()
{
  echo "***********************************************************************"
  echo "* This will install docker-engine"
  echo "* The following tasks will be performed:"
  echo "-----------------------------------------------------------------------"
  echo "* apt-get update -y"
  echo "* apt-get install -y apt-transport-https ca-certificates"
  echo "* apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D"
  echo "* echo "deb https://apt.dockerproject.org/repo ubuntu-$DISTRIB_CODENAME main" >> /etc/apt/sources.list.d/docker.list"
  echo "* apt-get update -y"
  echo "* apt-get install -y docker-engine"
  echo "* service docker start"
  echo "***********************************************************************"
  echo "* Do you want to install?"

  while true; do
      read -p "(y/n): " yn
      case $yn in
          [Yy]* )
            # Run update
            echo "***********************************************************************"
            echo "* Updating "
            echo "***********************************************************************"
            apt-get update

            echo "***********************************************************************"
            echo "* Installing tools"
            echo "***********************************************************************"
            apt-get install -y apt-transport-https ca-certificates

            echo "***********************************************************************"
            echo "* Installing keys"
            echo "***********************************************************************"
            apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

            # Get release
            . /etc/lsb-release

            # Check if apt-get entry already exists
            if [ -e "/etc/apt/sources.list.d/docker.list" ]; then
              echo "***********************************************************************"
              echo "* /etc/apt/sources.list.d/docker.list exists "
              echo "***********************************************************************"
            else
              echo "***********************************************************************"
              echo "* Register docker to apt-get "
              echo "***********************************************************************"
              echo "deb https://apt.dockerproject.org/repo ubuntu-$DISTRIB_CODENAME main" >> /etc/apt/sources.list.d/docker.list
            fi

            echo "***********************************************************************"
            echo "* Update "
            echo "***********************************************************************"
            apt-get update -y

            echo "***********************************************************************"
            echo "* Install docker-engine "
            echo "***********************************************************************"
            apt-get install -y docker-engine

            echo "***********************************************************************"
            echo "* Starting docker service "
            echo "***********************************************************************"

            service docker start

            break;;
            [Nn]* ) break;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

#*******************************************************************************

#
# Definition of function create_docker_group ()
#
# brief: Creates the group docker to run docker withput sudo

create_docker_group ()
{
  echo "***********************************************************************"
  echo "* If you want to run docker without sudo you can create a docker group"
  echo "* This script will perform these tasks"
  echo "-----------------------------------------------------------------------"
  echo "* groupadd docker"
  echo "* usermod -aG docker $USER"
  echo "***********************************************************************"
  echo "* Do you want to install?"

  while true; do
      read -p "(y/n): " yn
      case $yn in
        [Yy]* )

        echo "***********************************************************************"
        read -p "Please enter username: " USERNAME
        echo "***********************************************************************"

        echo "* groupadd docker"
        groupadd docker

        echo "* usermod -aG docker $USERNAME"
        usermod -aG docker $USERNAME

        echo "***********************************************************************"
        echo "* You've to log off and log in again to run docker without sudo"
        echo "***********************************************************************"

        break;;
        [Nn]* ) break;;
        * ) echo "Please answer yes or no.";;
      esac
  done
}

#*******************************************************************************

#
# Definition of function install_nvidia_docker ()
#
# brief: Installs nvidia docker

install_nvidia_docker ()
{
  echo "***********************************************************************"
  echo "* This will install nvidia-docker to your machine"
  echo "* The following tasks will be performed:"
  echo "-----------------------------------------------------------------------"
  echo "* apt-get install -y wget"
  echo "* wget -P /tmp https://github.com/NVIDIA/nvidia-docker/releases/download/v1.0.0-rc.3/nvidia-docker_1.0.0.rc.3-1_amd64.deb"
  echo "* dpkg -i /tmp/nvidia-docker*.deb && rm /tmp/nvidia-docker*.deb"
  echo "***********************************************************************"
  echo "* Do you want to install?"

  while true; do
      read -p "(y/n): " yn
      case $yn in
        [Yy]* )
          apt-get install -y wget
          wget -P /tmp https://github.com/NVIDIA/nvidia-docker/releases/download/v1.0.0-rc.3/nvidia-docker_1.0.0.rc.3-1_amd64.deb
          dpkg -i /tmp/nvidia-docker*.deb && rm /tmp/nvidia-docker*.deb

          echo "***********************************************************************"
          echo "* Running nvidia-docker test"
          nvidia-docker run --rm nvidia/cuda nvidia-smi
          echo "***********************************************************************"
        break;;
        [Nn]* ) break;;
        * ) echo "* Please answer yes or no.";;
      esac
  done
}

#*******************************************************************************

#
# Main script function
#
# brief: Runs the script



echo "***********************************************************************"
echo "* DOCKER INSTALL SCRIPT"
echo "-----------------------------------------------------------------------"
echo "* Version: $VERSION"
echo "* Author : $AUTHOR"
echo "* E-Mail : $EMAIL"
echo "* License: BSD"
echo "-----------------------------------------------------------------------"
echo "* This script will install the docker-engine and nvidia-docker to your"
echo "* PC. "
echo "***********************************************************************"

# Check for sudo rights
check_sudo

# Install docker
install_docker

# Create docker group to run docker without sudo
create_docker_group

# Install nvidia docker
install_nvidia_docker

echo "***********************************************************************"
echo " Install script FINISHED"
echo "***********************************************************************"

echo $RETURN
